## Server with REST API ##
The third task from [Binary studio](https://binary-studio.com/)

### Goals ###
- ✅ Create a repository on Bitbucket
- ✅ Use NPM to install and set up Node.js environment with Express.js and other dependencies
- ✅ Use new features of ES6 javascript for this project
- ❌ Write a server with API to meet following requirements
- ❌ Make sure that the data which comes is being verified

**GET: /user**
to get all user form the database

**GET: /user/:id**
to get one user by its ```ID```

**POST: /user**
to create a new user from the ```body``` which came with the request

**PUT: /user/:id**
to update a user's data using the data from ```body``` which came with the request

**DELETE: /user/:id**
to delete a user by its ```ID```